# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
require 'require_all'
require_relative '../config/config'

require 'byebug'
require 'factory_bot_rails'
require 'faker'
require 'rspec'
require 'timecop'

connect('sfmc')

require_all('models')

Faker::Config.locale = 'en-US'
require_all('spec/support/')

RSpec.configure do |config|
  #config.use_transactional_fixtures = false

  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    FactoryBot.find_definitions
  end

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  if ENV["CI"]
    config.before(:example, :focus) { raise "Failing the build because you have focused specs!" }
  else
    config.filter_run :focus
    config.run_all_when_everything_filtered = true
  end

  # Limits the available syntax to the non-monkey patched syntax that is
  # recommended. For more details, see:
  #   - http://myronmars.to/n/dev-blog/2012/06/rspecs-new-expectation-syntax
  #   - http://www.teaisaweso.me/blog/2013/05/27/rspecs-new-message-expectation-syntax/
  #   - http://myronmars.to/n/dev-blog/2014/05/notable-changes-in-rspec-3#new__config_option_to_disable_rspeccore_monkey_patching
  config.disable_monkey_patching!

  if config.files_to_run.one?
    config.default_formatter = 'doc'
  else
    config.profile_examples = 10
  end

  config.order = :random

  Kernel.srand config.seed
end
