def run_query(name)
  sql = File.open("queries/#{name}.sql").read

  # Because we're not dynamically passing in time to queries,
  # and because we can't (shouldn't) modify the time on SQL Server,
  # sub in instances of GETDATE with current system time
  if sql.upcase.include?("GETDATE()")
    sql.gsub!(
      /getdate\(\)/i,
      "CONVERT(DATETIME, '#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}')"
    )
  end

  return ActiveRecord::Base.connection.exec_query(sql)
end