FactoryBot.define do
  factory :customer do
    
      SubscriberKey { nil }
    
      Note { Faker::Superhero.name }
    
      PurchaseCount { Faker::Number.number(digits: 1) }
    
      PhoneNumber { Faker::PhoneNumber.phone_number }
    
  end
end