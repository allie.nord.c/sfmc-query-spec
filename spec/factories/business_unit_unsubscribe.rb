FactoryBot.define do
  factory :business_unit_unsubscribe do
    BusinessUnitID { Faker::Number.number(digits: 4) }
    subscriber
    SubscriberKey { subscriber.SubscriberKey }
    UnsubDateUTC { Time.now }
  end
end