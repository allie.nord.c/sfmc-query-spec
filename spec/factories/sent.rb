FactoryBot.define do
  factory :sent do
    transient do
      with_associations { [] }
      with_subscribers { nil }
    end

    job
    AccountID { job.AccountID }
    ListID { Faker::Number.number(digits: 4) }
    BatchID { Faker::Number.number(digits: 4) }
    subscriber
    SubscriberKey { subscriber.SubscriberKey }
    EventDate { Time.now }
    Domain { subscriber.EmailAddress.gsub(/.+@([^.]+).+/, '\1') }

    after(:create) do |sent, evaluator|
      %i[open click].each do |ass|
        if evaluator.with_associations.include?(ass)
          create(
            ass,
            job: sent.job,
            ListID: sent.ListID,
            BatchID: sent.BatchID,
            subscriber: sent.subscriber
          )
        end
      end
    end
  end
end