FactoryBot.define do
  factory :open do
    job
    AccountID { job.AccountID }
    ListID { Faker::Number.number(digits: 4) }
    BatchID { Faker::Number.number(digits: 4) }
    subscriber
    SubscriberKey { subscriber.SubscriberKey }
    EventDate { Time.now }
    Domain { subscriber.EmailAddress.gsub(/.+@([^.]+).+/, '\1') }
  end
end