FactoryBot.define do
  factory :policy__c do
    
      SubscriberKey { nil }
    
      CancellationDate { Time.now }
    
  end
end