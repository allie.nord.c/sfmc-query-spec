FactoryBot.define do
  factory :subscriber do
    email = Faker::Internet.email
    EmailAddress { email }
    SubscriberKey { email }
    SubscriberType { 0 }
    Status { 'active' }
    transient do
      bounced { false }
      bounced_at { nil }
      unsubscribed { false }
      unsubscribed_at { nil }
      with_email_history { true }
      email_history_starts_at { nil }
      emails_count { rand(30) }
    end

    after(:create) do |subscriber, evaluator|
      if evaluator.bounced
        bounced_at = evaluator.bounced_at || Time.now
        create(:bounce, subscriber: subscriber, EventDate: bounced_at)
        subscriber.update!(BounceCount: 1, DateUndeliverable: bounced_at)
      end

      if evaluator.unsubscribed
        unsubscribed_at = evaluator.unsubscribed_at || Time.now
        create(:unsubscribe, subscriber: subscriber, EventDate: unsubscribed_at)
        subscriber.update!(DateUnsubscribed: unsubscribed_at)
      end

      if evaluator.with_email_history
        min_date = evaluator.email_history_starts_at || 6.months.ago
        max_date = evaluator.bounced_at || evaluator.unsubscribed_at || Time.now

        # Initial email is at history start
        create(
          :sent, 
          subscriber: subscriber,
          EventDate: min_date,
          with_associations: %i[open click]
        )
        (evaluator.emails_count - 1).times do |index|
          create(
            :sent, 
            subscriber: subscriber,
            EventDate: min_date + rand((max_date.to_date - min_date.to_date).to_i).days,
            with_associations: %i[open click]
          )
        end
      end
    end
  end
end
