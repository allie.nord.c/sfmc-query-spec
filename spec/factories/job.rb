FactoryBot.define do
  factory :job do
    EmailID { Faker::Number.number(digits: 4) }
    AccountID { Faker::Number.number(digits: 4) }
    AccountUserID { Faker::Number.number(digits: 4) }
    FromName { Faker::Name.name }
    FromEmail { Faker::Internet.email }
    EmailName { Faker::FunnyName.name }
    EmailSubject { Faker::Marketing.buzzwords }
    CreatedDate { Time.now }
    IsMultipart { false }
    IsWrapped { false }
    ResolveLinksWithCurrentData { true }
    DeduplicateByEmail { true }
    SalesForceTotalSubscriberCount { 0 }
    SalesForceErrorSubscriberCount { 0 }
    SuppressTracking { false }
  end
end