require 'spec_helper'

RSpec.describe Subscriber do
  it "is not unsubscribed" do
    subscriber = create(:subscriber, with_email_history: true)
    pp "Sent: #{subscriber.sent.count}"
    pp "Opens: #{subscriber.opens.count}"
    pp "Clicks: #{subscriber.clicks.count}"
    expect(subscriber.DateUnsubscribed).to be_nil
  end
end
