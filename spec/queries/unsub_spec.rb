require 'spec_helper'

RSpec.describe "unsub" do
  subject { "unsub" }
  let(:query) do
    run_query(subject)
  end

  context "on different dates of interest" do
    dates = [Time.new('2021-01-01'), Time.new('2021-03-01'), 3.days.ago]

    dates.each do |date|
      it "counts unsubed within 3 - 5 days before query date #{date}" do
        Timecop.freeze(date) do
          create(:subscriber, DateUnsubscribed: 3.days.ago)
          create(:subscriber, DateUnsubscribed: 5.days.ago)

          expect(query.count).to eq(1)
          expect(query[0]["UNSUB_COUNT"]).to eq(2)
        end
      end

      it "does not count unsubed >5 days before query date #{date}" do
        Timecop.freeze(date) do
          create(:subscriber, DateUnsubscribed: 6.days.ago)

          expect(query.count).to eq(1)
          expect(query[0]["UNSUB_COUNT"]).to eq(0)
        end
      end

      it "does not count unsubed <2 days before query date #{date}" do
        Timecop.freeze(date) do
          create(:subscriber, DateUnsubscribed: 2.days.ago)

          expect(query.count).to eq(1)
          expect(query[0]["UNSUB_COUNT"]).to eq(0)
        end
      end

      it "does not count subscribers who are not unsubed #{date}" do
        create(:subscriber)

        time_ranges = [2, 3, 5, 6]
        time_ranges.each do |time_range|
          Timecop.freeze(date - (time_range).days) do
            create(:subscriber)
          end
        end

        Timecop.freeze(date) do
          expect(query.count).to eq(1)
          expect(query[0]["UNSUB_COUNT"]).to eq(0)
        end
      end
    end
  end
end
