class Open < ApplicationRecord
  self.table_name = "ent._open"

  belongs_to :job, foreign_key: "JobID"
  belongs_to :subscriber, foreign_key: "SubscriberID"

  def sent
    Sent.find_by(
      JobID: self.JobID,
      ListID: self.ListID,
      BatchID: self.BatchID,
      SubscriberID: self.SubscriberID
    )
  end
end