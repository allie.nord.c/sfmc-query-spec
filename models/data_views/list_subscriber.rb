class ListSubscriber < ApplicationRecord
  self.table_name = "ent._listsubscribers"

  has_one :subscriber, foreign_key: "SubscriberID"
end