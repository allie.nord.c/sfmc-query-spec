class Subscriber < ApplicationRecord
  self.table_name = "ent._Subscribers"
  self.primary_key = "SubscriberID"

  has_many :list_subscribers, foreign_key: "SubscriberID"
  has_many :bounces, foreign_key: "SubscriberID"
  has_many :business_unit_unsubscribes, foreign_key: "SubscriberID"
  has_many :clicks, foreign_key: "SubscriberID"
  has_many :complaints, foreign_key: "SubscriberID"
  has_one :enterprise_attribute, foreign_key: "_SubscriberID"
  has_many :ftafs, foreign_key: "SubscriberID"
  has_many :opens, foreign_key: "SubscriberID"
  has_many :sent, foreign_key: "SubscriberID"
  has_many :sms_message_trackings, foreign_key: "SubscriberID"
  has_many :sms_subscription_logs, foreign_key: "SubscriberID"
  has_many :unsubscribes, foreign_key: "SubscriberID"

  enum subscribertype: {
    exacttarget: 0,
    salesforce_lead: 1,
    salesforce_contact: 2,
    unknown: 3
  }
end