class EnterpriseAttribute < ApplicationRecord
  self.table_name = "ent._EnterpriseAttribute"
  self.primary_key = "_SubscriberID"

  belongs_to :subscriber, foreign_key: "SubscriberID"
end