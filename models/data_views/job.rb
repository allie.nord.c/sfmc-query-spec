class Job < ApplicationRecord
  self.table_name = "ent._job"
  self.primary_key = "JobID"

  has_many :bounces, foreign_key: "JobID"
  has_many :clicks, foreign_key: "JobID"
  has_many :complaints, foreign_key: "JobID"
  has_many :ftafs, foreign_key: "JobID"
  has_many :opens, foreign_key: "JobID"
  has_many :sents, foreign_key: "JobID"
  has_many :unsubscribes, foreign_key: "JobID"
end