class SMSMessageTracking < ApplicationRecord
  self.table_name = "ent._smsmessagetracking"

  belongs_to :subscriber, foreign_key: "SubscriberID"
end