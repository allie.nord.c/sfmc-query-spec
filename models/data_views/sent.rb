class Sent < ApplicationRecord
  self.table_name = "ent._sent"

  belongs_to :job, foreign_key: "JobID"
  belongs_to :subscriber, foreign_key: "SubscriberID"

  def opens
    Open.where(
      "JobID = ? AND ListID = ? AND BatchID = ? AND SubscriberID = ?",
      self.JobID, self.ListID, self.BatchID, self.SubscriberID
    )
  end
end