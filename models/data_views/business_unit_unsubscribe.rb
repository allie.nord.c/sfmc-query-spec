class BusinessUnitUnsubscribe < ApplicationRecord
  self.table_name = "ent._businessunitunsubscribes"

  belongs_to :subscriber, foreign_key: "SubscriberID"
end