class SMSSubscriptionLog < ApplicationRecord
  self.table_name = "ent._smssubscriptionlog"

  belongs_to :subscriber, foreign_key: "SubscriberKey"
end