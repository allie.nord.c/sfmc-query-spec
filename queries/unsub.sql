SELECT COUNT(*) AS UNSUB_COUNT
FROM ent._Subscribers
WHERE DATEDIFF(day, DateUnsubscribed, GETDATE()) <= 5
AND DATEDIFF(day, DateUnsubscribed, GETDATE()) >= 3