require_relative '../migration_base'

class CreateFtaf < MigrationBase
  def change
    create_table 'ent.[_FTAF]', id: false do |t|
      t.integer :AccountID, null: false
      t.integer :OYBAccountID
      t.integer :JobID, null: false
      t.integer :ListID, null: false
      t.integer :BatchID, null: false
      t.integer :SubscriberID, null: false
      t.string :SubscriberKey, limit: 154, null: false
      t.datetime :TransactionTime, null: false
      t.boolean :IsUnique, null: false
      t.string :Domain, limit: 128, null: false
      t.string :TriggererSendDefinitionObjectID, limit: 36
      t.string :TriggeredSendCustomerKey, limit: 36
    end
  end
end