require_relative '../migration_base'

class CreateJourneyActivity < MigrationBase
  def change
    create_table 'ent.[_JourneyActivity]', id: false do |t|
      t.string :VersionID, limit: 36, null: false
      t.string :ActivityID, limit: 36, null: false, unique: true
      t.string :ActivityName, limit: 200
      t.string :ActivityExternalKey, limit: 200, null: false
      t.string :JourneyActivityObjectID, limit: 36
      t.string :ActivityType, limit: 512
    end
  end
end