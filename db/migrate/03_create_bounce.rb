require_relative '../migration_base'

class CreateBounce < MigrationBase
  def change
    create_table 'ent.[_Bounce]', id: false do |t|
      t.integer :AccountID, null: false
      t.integer :OYBAccountID
      t.integer :JobID, null: false
      t.integer :ListID, null: false
      t.integer :BatchID, null: false
      t.integer :SubscriberID, null: false
      t.string :SubscriberKey, limit: 154, null: false
      t.datetime :EventDate, null: false
      t.boolean :IsUnique, null: false
      t.string :Domain, limit: 128, null: false
      t.integer :BounceCategoryID, null: false
      t.string :BounceCategory, limit: 50
      t.integer :BounceSubcategoryID
      t.string :BounceSubcategory, limit: 50
      t.integer :BounceTypeID
      t.string :BounceType, limit: 50
      t.string :SMTPBounceReason
      t.string :SMTPMessage
      t.integer :SMTPCode
      t.string :TriggererSendDefinitionObjectID, limit: 36
      t.string :TriggeredSendCustomerKey, limit: 36
    end
  end
end