require_relative '../migration_base'

class CreateUnsubscribe < MigrationBase
  def change
    create_table 'ent.[_Unsubscribe]', id: false do |t|
      t.integer :AccountID, null: false
      t.integer :OYBAccountID
      t.integer :JobID, null: false
      t.integer :ListID, null: false
      t.integer :BatchID, null: false
      t.integer :SubscriberID, null: false
      t.string :SubscriberKey, limit: 154, null: false
      t.datetime :EventDate, null: false
      t.string :Domain, limit: 128, null: false
      t.boolean :IsUnique, null: false
    end
  end
end