require_relative '../migration_base'

class CreateCoupon < MigrationBase
  def change
    create_table 'ent.[_Coupon]', id: false do |t|
      t.string :Name, limit: 128, null: false
      t.string :ExternalKey, limit: 36, null: false
      t.string :Description, null: false
      t.datetime :BeginDate, null: false
      t.datetime :ExpirationDate, null: false
    end
  end
end