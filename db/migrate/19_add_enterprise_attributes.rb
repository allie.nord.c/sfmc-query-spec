require 'json'
require_relative '../migration_base'

class AddEnterpriseAttributes < MigrationBase
  def change
    sfmc_schema = JSON.parse(File.read('db/sfmc_schema/enterprise_attributes.json'))
    ent_attr = sfmc_schema["Attributes"]

    ent_attr.each do |field|
      add_column 'ent.[_EnterpriseAttribute]', field['Name'], field['DataType'].to_sym, limit: field['Length']&.to_i
    end
  end
end
