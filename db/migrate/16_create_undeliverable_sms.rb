require_relative '../migration_base'

class CreateUndeliverableSms < MigrationBase
  def change
    create_table 'ent.[_UndeliverableSMS]', id: false do |t|
      t.string :MobileNumber, limit: 15, null: false
      t.boolean :Undeliverable, null: false
      t.integer :BounceCount, null: false
      t.datetime :FirstBounceDate, null: false
      t.datetime :HoldDate
    end
  end
end