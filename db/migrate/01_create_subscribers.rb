require_relative '../migration_base'

class CreateSubscribers < MigrationBase
  def change
    create_table 'ent.[_Subscribers]', id: false do |t|
      t.integer :SubscriberID, primary_key: true, null: false, unique: true, is_identity: true
      t.datetime :DateUndeliverable
      t.datetime :DateJoined
      t.datetime :DateUnsubscribed
      t.datetime :Domain, limit: 254
      t.string :EmailAddress, limit: 254, null: false, index: true
      t.integer :BounceCount, null: false, default: 0
      t.string :SubscriberKey, limit: 254, null: false, index: true
      t.string :SubscriberType, limit: 100, null: false
      t.string :Status, limit: 12
      t.integer :Locale
    end
  end
end

