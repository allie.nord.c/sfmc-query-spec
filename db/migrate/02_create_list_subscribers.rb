require_relative '../migration_base'

class CreateListSubscribers < MigrationBase
  def change
    create_table 'ent.[_ListSubscribers]', id: false do |t|
      t.integer :AddedBy, null: false
      t.string :AddMethod, limit: 17, null: false
      t.datetime :CreatedDate
      t.datetime :DateUnsubscribed
      t.string :EmailAddress, limit: 254
      t.integer :ListID
      t.string :ListName, limit: 50
      t.string :ListType, limit: 16, null: false
      t.string :Status, limit: 12
      t.integer :SubscriberID
      t.string :SubscriberKey, limit: 254
      t.string :SubscriberType, limit: 100
    end
  end
end
