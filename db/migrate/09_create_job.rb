require_relative '../migration_base'

class CreateJob < MigrationBase
  def change
    create_table 'ent.[_Job]', id: false do |t|
      t.integer :JobID, null: false, primary_key: true, is_identity: true
      t.integer :EmailID
      t.integer :AccountID
      t.integer :AccountUserID
      t.string :FromName, limit: 130
      t.string :FromEmail, limit: 100
      t.datetime :SchedTime
      t.datetime :PickupTime
      t.datetime :DeliveredTime
      t.string :EventID, limit: 50
      t.boolean :IsMultipart, null: false
      t.string :JobType, limit: 50
      t.string :JobStatus, limit: 50
      t.integer :ModifiedBy
      t.datetime :ModifiedDate
      t.string :EmailName, limit: 100
      t.string :EmailSubject, limit: 200
      t.boolean :IsWrapped, null: false
      t.string :TestEmailAddr, limit: 128
      t.string :Category, limit: 100
      t.string :BccEmail, limit: 100
      t.datetime :OriginalSchedTime
      t.datetime :CreatedDate, null: false
      t.string :CharacterSet, limit: 30
      t.string :IPAddress, limit: 50
      t.integer :SalesForceTotalSubscriberCount, null: false
      t.integer :SalesForceErrorSubscriberCount, null: false
      t.string :SendType, limit: 128
      t.string :DynamicEmailSubject
      t.boolean :SuppressTracking, null: false
      t.string :SendClassificationType, limit: 32
      t.string :SendClassification, limit: 36
      t.boolean :ResolveLinksWithCurrentData, null: false
      t.string :EmailSendDefinition, limit: 36
      t.boolean :DeduplicateByEmail, null: false
      t.string :TriggererSendDefinitionObjectID, limit: 36
      t.string :TriggeredSendCustomerKey, limit: 36
    end
  end
end