require 'json'
require_relative '../migration_base'
require_relative '../util/field_generator'

class CreateDataExtensions < MigrationBase
  def change
    sfmc_schema = JSON.parse(File.read('db/sfmc_schema/data_extensions.json'))
    data_extensions = sfmc_schema["DataExtensions"]
    primary_keys = {}

    data_extensions.each do |de|
      table_name = "bu.#{de["Name"]}"
      table_primary_keys = []

      create_table table_name, id: false do |t|
        de["Fields"].each do |field|
          column_name, column_type, args = field_generator(field)
          table_primary_keys.append(column_name) if field["IsPrimaryKey"]
          t.send(column_type.to_sym, column_name, **args)
        end
      end

      primary_keys[table_name] = table_primary_keys if table_primary_keys.present?
    end

    primary_keys.each do |table_name, primary_keys|
      execute <<~"SQL".squish
        ALTER TABLE #{table_name}
        ADD CONSTRAINT uc_#{table_name} UNIQUE (#{primary_keys.join','})
      SQL
    rescue ActiveRecord::StatementInvalid => exception
      if exception.message == "TinyTds::Error: Incorrect syntax near '.'."
        next
      else
        raise exception
      end
    end
  end
end
