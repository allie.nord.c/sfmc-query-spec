require_relative '../migration_base'

class CreateSmsSubscriptionLog < MigrationBase
  def change
    create_table 'ent.[_SMSSubscriptionLog]', id: false do |t|
      t.datetime :LogDate
      t.string :SubscriberKey, limit: 254, null: false
      t.integer :MobileSubscriptionID, null: false
      t.string :SubscriptionDefinitionID, null: false
      t.string :MobileNumber, null: false
      t.integer :OptOutStatusID
      t.integer :OptOutMethodID
      t.datetime :OptOutDate
      t.integer :OptInStatusID, null: false
      t.integer :OptInMethodID
      t.datetime :OptInDate
      t.integer :Source
      t.datetime :CreatedDate, null: false
      t.datetime :ModifiedDate, null: false
    end
  end
end