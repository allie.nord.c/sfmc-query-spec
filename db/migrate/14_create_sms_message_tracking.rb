require_relative '../migration_base'

class CreateSmsMessageTracking < MigrationBase
  def change
    create_table 'ent.[_SMSMessageTracking]', id: false do |t|
      t.integer :MobileMessageTrackingID, primary_key: true, is_identity: true
      t.integer :EID
      t.integer :MID
      t.string :Mobile, limit: 15, null: false
      t.integer :MessageID
      t.string :KeywordID
      t.string :CodeID
      t.string :ConversationID
      t.integer :CampaignID
      t.boolean :Sent, null: false
      t.boolean :Delivered
      t.boolean :Undelivered
      t.integer :Unsub
      t.boolean :OptIn
      t.boolean :OptOut
      t.boolean :Outbound
      t.boolean :Inbound
      t.datetime :CreateDateTime, null: false
      t.datetime :ModifiedDateTime, null: false
      t.datetime :ActionDateTime, null: false
      t.string :MessageText, limit: 160
      t.boolean :IsTest
      t.integer :MobileMessageRecurrenceID
      t.integer :ResponseToMobileMessageTrackingID
      t.boolean :IsValid
      t.integer :InvalidationCode
      t.integer :SendID
      t.integer :SendSplitID
      t.integer :SendSegmentID
      t.integer :SendJobID
      t.integer :SendGroupID
      t.integer :SendPersonID
      t.integer :SubscriberID
      t.string :SubscriberKey, limit: 254
      t.integer :SMSStandardStatusCodeId
      t.string :Description
      t.string :Name
      t.string :ShortCode
      t.string :SharedKeyword
      t.integer :Ordinal
      t.string :FromName, limit: 11
      t.string :JBActivityID, limit: 36
      t.string :JBDefinitionID, limit: 36
    end
  end
end