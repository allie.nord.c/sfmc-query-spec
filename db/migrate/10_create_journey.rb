require_relative '../migration_base'

class CreateJourney < MigrationBase
  def change
    create_table 'ent.[_Journey]', id: false do |t|
      t.string :VersionID, limit: 36, null: false, unique: true
      t.string :JourneyID, limit: 36, null: false, unique: true
      t.string :JourneyName, limit: 200, null: false
      t.integer :VersionNumber, null: false
      t.datetime :CreatedDate, null: false
      t.datetime :LastPublishedDate
      t.datetime :ModifiedDate, null: false
      t.string :JourneyStatus, limit: 100, null: false
    end
  end
end