require_relative '../migration_base'

class CreateEnterpriseAttribute < MigrationBase
  def change
    create_table 'ent.[_EnterpriseAttribute]', id: false do |t|
      t.integer :_SubscriberID, primary_key: true, null: false, unique: true
    end
  end
end

