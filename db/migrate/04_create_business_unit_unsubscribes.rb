require_relative '../migration_base'

class CreateBusinessUnitUnsubscribes < MigrationBase
  def change
    create_table 'ent.[_BusinessUnitUnsubscribes]', id: false do |t|
      t.integer :BusinessUnitID, null: false
      t.integer :SubscriberID, null: false
      t.string :SubscriberKey, limit: 254, null: false
      t.datetime :UnsubDateUTC
      t.string :UnsubReason, limit: 100
    end
  end
end
