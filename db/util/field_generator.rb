# Translates SFMC field types into Rails-friendly column types
def field_generator(field)
  column_name = field['Name']
  add_column_args = {
    limit: field['MaxLength']&.to_i,
    default: default_value(field['DefaultValue']),
    null: !field["IsPrimaryKey"] && !field['Nullable']
  }

  case field["FieldType"]
  when "Text"
    column_type = :string
  when "Number"
    column_type = :integer
  when "Date"
    column_type = :datetime
  when "Email"
    column_type = :string
    add_column_args[:limit] = 254
  when "Phone"
    column_type = :string
    add_column_args[:limit] = 15
  when "Decimal"
    column_type = :decimal
  when "Locale"
    column_type = :string
    add_column_args[:limit] = 5
  else
    raise Exception, "Unknown DataType #{field["DataType"]}"
  end

  return column_name, column_type, add_column_args
end

def default_value(value)
  if value.present?
    value
  else
    nil
  end
end
