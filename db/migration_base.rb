class MigrationBase < ActiveRecord::Migration[6.0]
  def migrate(*)
    begin
      super
    rescue ActiveRecord::StatementInvalid => exception
      unless exception.message == "TinyTds::Error: Incorrect syntax near '.'."
        raise exception
      end
    end
  end
end