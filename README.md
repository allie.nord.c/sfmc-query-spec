# SFMC Query Specs

## Prerequisites

- Homebrew (recommended, Mac only)
  + This will make everything below much easier
- Ruby
  + See `.ruby-version` for current version
  + Recommended to use rbenv to manage versions:
    - Mac: 
    - Windows: 
- Bundler >= 2.0
  + Run `gem install bundler`
- Docker
  + Mac: 
  + Windows: 
- Docker Compose
  + Mac
  + Windows
- FreeTDS (used to interact with SQL Server)
  + Mac: 
  + Windows: 

Linux: all of the above, figure it out yourself (hey, you're the one who chose to run Linux)

## Setup

- Install required Ruby gems
  + `bundle install`
- Setup database
  + `bin/setup`

## Adding Data Extensions and Enterprise Subscriber Attributes

### Without Installed Package

- Copy the contents of `cloud_pages/schema.ssjs` to a new Cloud Pages JS code resource
- Edit the variable `targetDataExtensions` to include the **names** of the data extensions to include
  + Subscriber attributes will be included automatically
- Save and publish the resource
- Navigate to the resource URL
- Copy the output into the file `db/schema.json`
- Run `bin/setup`

### With Installed Package

*Coming soon?*

### Notes

- Removing data extensions
  + If removing data extensions from the `targetDataExtensions` list, be sure to also remove any tests that may depend on those DEs

## Notes

- Database is not intended for long-term storage - each setup/teardown cycle will completely erase any data
  + Instead, rely on built-in helpers to create test records as needed

## TODO

Size: S/M/L
Impact (low to high): 1/2/3

- ~DateTime faking with TimeCop + dynamic query substitution - M+3~
- ~Model associations for data views - S+2~
- More data view factories:
  + ~Subscriber with email history - M+3~
  + ~Bounced subscriber with history - S+1~
  + ~Unsubscribed subscriber with history - S+1~
- ~Auto-create models for data extensions - M+3~
- Auto-associate sendable data extensions with Subscriber - M+3
- ~Auto-create factories for data extensions - L+3~
- ~Rake task to regenerate a data extension model + factory - S+1~
- Rake task to generate a new query spec from template - S+2
- SSJS Cloud Page for schema generation - M+3
- Query config file - L+1
- Guard config to live-test query changes - S+3
- CI/CD example - M+1
- Multiple BUs
- Extend Subscriber with EnterpriseAttribute - S+1
  + Might need to ask GLV about this one
- Real-life examples:
  + GN - Abandon Cart - M+3
  + GN - EOM Query - M+3

## Notes for engineers

If you're at all familiar with Rails or Active Record, or general database best practices, then this repo will horrify you.

This is because we're attempting to replicate the environment of SFMC:

- System data views are not designed with DB best practices - i.e., foreign key relations are not always well-designed, column names use proper-case
- Data extensions can be designed with varying levels of configuration:
  + Proper-case and snake-case column names mixed
  + Primary key is not always present
