require 'activerecord-sqlserver-adapter'

BASE_CONNECTION = {
  adapter: 'sqlserver',
  host: 'localhost',
  port: '1433'
}

SA_BASE = {
  **BASE_CONNECTION,
  username: 'sa',
  password: 'welcome@1'
}

SA_MASTER = {
  **SA_BASE,
  database: 'master'
}

SA_SFMC = {
  **SA_BASE,
  database: 'sfmc'
}

SFMC = {
  **BASE_CONNECTION,
  username: 'sfmc_login',
  password: 'welcome@1',
  database: 'sfmc'
}

def connect(database = 'sfmc')
  db_config = if database == 'master'
    SA_MASTER
  elsif database == 'sa_sfmc'
    SA_SFMC
  elsif database == 'sfmc'
    SFMC
  else
    raise Exception("Unknown database '#{database}'")
  end

  ActiveRecord::Base.establish_connection(db_config)
end
