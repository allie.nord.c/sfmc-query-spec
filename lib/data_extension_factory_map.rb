class DataExtensionFactoryMap
  attr_accessor :field

  def self.run(field)
    new(field).value
  end

  def initialize(field)
    @field = field
  end

  def value
    "#{field_name} { #{faker_value} }"
    
  end

  def field_type
    @field_type ||= field[:FieldType]
  end

  def field_name
    @field_name ||= field[:Name]
  end

  def field_name_d
    @field_name_d ||= field_name.downcase
  end

  def faker_value
    case field_type
    when "EmailAddress"
      "Faker::Internet.email"
    when "Phone"
      "Faker::PhoneNumber.phone_number"
    when "Date"
      "Time.now"
    when "Number"
      "Faker::Number.number(digits: 1)"
    when "Decimal"
      "Faker::Number.number(digits: 1)"
    when "Boolean"
      "Faker::Boolean.boolean"
    when "Locale"
      "en-us"
    when "Text"
      faker_text_value
    else
      "nil"
    end
  end

  def faker_text_value
    if ["firstname", "first_name", "fname", "first name"].include?(field_name_d)
      "Faker::Name.first_name"
    elsif ["lastname", "last_name", "lname", "last name"].include?(field_name_d)
      "Faker::Name.last_name"
    elsif ["name", "fullname", "full_name", "full name"].include?(field_name_d)
      "Faker::Name.name"
    elsif field_name_d.include?("url")
      "Faker::Internet.url"
    elsif ["contactid", "leadid", "salesforceid", "contact_id", "lead_id", "salesforce_id", "contact id", "lead id", "salesforce id"].include?(field_name_d)
      "Faker::Alphanumeric.alpha(number: 18)"
    elsif ["street1", "street 1", "street_1", "street_address", "street address", "streetaddress"].include?(field_name_d)
      "Faker::Address.street_address"
    elsif ["street2", "street 2", "street_2", "secondary address", "secondaryaddress", "secondary_address"].include?(field_name_d)
      "Faker::Address.secondary_address"
    elsif field_name_d == "city"
      "Faker::Address.city"
    elsif ["zipcode", "zip_code", "zip code", "zip", "postalcode", "postal_code", "postal code"].include?(field_name_d)
      "Faker::Address.zip"
    elsif field_name_d == "country"
      "us"
    elsif field_name_d == "state"
      "Faker::Address.state_abbr"
    elsif ["address", "mailing_address", "mailing address"].include?(field_name_d)
      "Faker::Address.full_address"
    else
      "Faker::Superhero.name"
    end
  end
end