require 'erb'
require 'json'
require_relative 'data_extension_factory_map'

class DataExtensionModelGenerator
  DE_SCHEMA_PATH = "db/sfmc_schema/data_extensions.json"
  FACTORY_PATH = "spec/factories"
  MODEL_PATH = "models"

  attr_accessor :raw_name, :is_sendable, :fields

  def self.run(force = false)
    config = JSON.parse(File.read('db/sfmc_schema/data_extensions.json'), symbolize_names: true)

    config[:DataExtensions].each do |de|
      new(de, force).run
    end
  end

  def initialize(config, force = false)
    @force = force
    @raw_name = config[:Name]
    @is_sendable = config[:IsSendable]
    @fields = config[:Fields]
    @file_name = @raw_name.parameterize.gsub("-", "_")
    @model_name = @file_name.camelize
  end

  def run
    create_model
    create_factory
  end

  def create_file(path, template)
    return if File.exist?(path) && !@force

    File.open(path, "w") do |f|
      f.write(ERB.new(template).result( binding ))
    end
  end

  def create_model
    path = "#{MODEL_PATH}/#{@file_name}.rb"
    template = File.read('lib/templates/model.erb')
    create_file(path, template)
  end

  def create_factory
    path = "#{FACTORY_PATH}/#{@file_name}.rb"
    template = File.read('lib/templates/factory.erb')
    create_file(path, template)
  end
end
